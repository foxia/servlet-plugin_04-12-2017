package com.example.plugins.tutorial.crud.api;

public interface MyPluginComponent
{
    String getName();
}